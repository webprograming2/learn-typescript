let w: unknown = 1;
w = "string";
w = {
    runANonExitenMethod: () => {
        console.log("I think therefore I am");
    }
} as { runANonExitenMethod: () => void }

if(typeof w == 'object' && w != null){
    (w as {runANonExitenMethod: Function}).runANonExitenMethod();
}